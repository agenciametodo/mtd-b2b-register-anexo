import React, { useState } from 'react'


import './styles.global.css'

const RepresentativeComponent = () => {

  const [nomeRepressentante, setNomeRepressentante] = useState('')
  const [email, setEmail] = useState('')
  const [telefone, setTelefone] = useState('')
  const [cidade, setCidade] = useState('')
  const [estado, setEstado] = useState('')
  const [marcas, setMarcas] = useState('')

  const [cadastroSucesso, setCadastroSucesso] = useState(false)
  const [erroCadastro, seterroCadastro] = useState(false)


  const requestOptions = {
    method: 'POST',
   // method: 'PATCH',
    headers: {
      Accept: 'application/vnd.vtex.ds.v10+json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      nome: nomeRepressentante,
      telefone: `+55${telefone.replace(/[^a-zA-Z0-9]/g, '')}`,
      email,
      estado:estado,
      cidade:cidade,
      marcas:marcas
    }),
  }



  const requestRegistrationRE = async () => {
    await fetch('/api/dataentities/RE/documents', requestOptions).then((res) => {
      if (res.ok) {
        setCadastroSucesso(true)
      } else {
        seterroCadastro(true)
      }
    })
  }


  const handleChangePhone = (event: any) => {
    let { value } = event.target

    value = value.replace(/\D/g, '')
    value = value.replace(/(\d{0})(\d)/, '$1($2')
    value = value.replace(/(\d{2})(\d)/, '$1) $2')
    value = value.replace(/(\d{5})(\d)/, '$1-$2')
    setTelefone(value)
  }

  if (erroCadastro) {
    return (
      <>
        <div className="component">
          <div className="component-content--registration">
            <h2 className="component-content--registration--title">
              Ocorreu um erro
            </h2>
            <p className="component-content--registration--text">
              Por favor entre em contato com nossa Central de
              Atendimento.
            </p>
          </div>
        </div>
      </>
    )
  }

  return (
    <>
      <div className="component">
        {!cadastroSucesso ? (
          <>
            <div className="component-content">
              <div className="component-content--top">
                <h2 className="component-content--title">Cadastro de representante</h2>
                <p className="component-content--text">
                  Insira os dados abaixo
                </p>

                <form encType="multipart/form-data"
                  className="component-content--form"
                  onSubmit={(event) => {
                    event.preventDefault()
                    requestRegistrationRE()
                  }}
                >
                  <div className="component-content">
                    <label
                      htmlFor="nomeRepressentante"
                      className="component-content-label"
                    >
                      Nome*
                    </label>
                    <input
                      id="nomeRepressentante"
                      name="nomeRepressentante"
                      className="component-content-input"
                      type="text"
                      placeholder="Digite o seu nome"
                      value={nomeRepressentante}
                      onChange={(event) => {
                        setNomeRepressentante(event.target.value)
                      }}
                      required
                    />
                  </div>

                  <div className="component-content">
                    <label htmlFor="email" className="component-content-label">
                      E-mail*
                    </label>
                    <input
                      id="email"
                      name="email"
                      className="component-content-input"
                      type="email"
                      placeholder="Digite o seu e-mail"
                      value={email}
                      onChange={(event) => {
                        setEmail(event.target.value)
                      }}
                      required
                    />
                  </div>

                  <div className="component-content">
                    <label htmlFor="telefone" className="component-content-label">
                      Telefone*
                    </label>
                    <input
                      id="telefone"
                      name="telefone"
                      className="component-content-input"
                      type="phone"
                      maxLength={15}
                      placeholder="(11) 99999-9999*"
                      value={telefone}
                      required
                      onChange={(e) => handleChangePhone(e)}
                    />
                  </div>


                  <div className="component-content">
                    <label htmlFor="cidade" className="component-content-label">
                      Cidade*
                    </label>
                    <input
                      id="cidade"
                      name="cidade"
                      className="component-content-input"
                      type="text"
                      required
                      placeholder="Digite a cidade"
                      value={cidade}
                      onChange={(event) => {
                        setCidade(event.target.value)
                      }}
                    />
                  </div>

                  <div className="component-content">
                    <label htmlFor="estado" className="component-content-label">
                      Estado*
                    </label>
                    <input
                      id="estado"
                      name="estado"
                      className="component-content-input"
                      type="text"
                      required
                      placeholder="Digite a estado"
                      value={estado}
                      onChange={(event) => {
                        setEstado(event.target.value)
                      }}
                    />
                  </div>

                  <div className="component-content">
                    <label htmlFor="marcas" className="component-content-label">
                    Interesse em representar quais categorias?*<span> ( Papelaria, Suprimento, Impressão, Áudio e vídeo, Presentes, Casa e segurança, Informática )</span>
                    </label>
                    <textarea
                      id="marcas"
                      name="marcas"
                      className="component-content-textarea"
                      value={marcas}
                      required
                      rows={5}
                      cols={5}
                      onChange={(event) => {
                        setMarcas(event.target.value)
                      }}
                    />
                  </div>

                  <p>
                    Ao clicar em confirmar concordo que aceito aos Termos e
                    Condições de Uso do Ecommerce; Termo Gerais da Política de
                    Privacidade
                  </p>
                  <button type="submit" className="component-content-button">
                    Confirmar
                  </button>
                </form>
              </div>
            </div>
          </>
        ) : (
          <>
            <div className="component-content--registration">
              <h2 className="component-content--registration--title">
                Dados enviados
              </h2>
              <p className="component-content--registration--text">
                Em breve damos um retorno
              </p>
            </div>
          </>
        )}
      </div>
    </>
  )
}

export default RepresentativeComponent
