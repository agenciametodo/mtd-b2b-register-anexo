/* eslint-disable react-hooks/exhaustive-deps */
import { useState, useEffect } from 'react'
import { useOrderForm } from 'vtex.order-manager/OrderForm'

const requestOptionsGET = {
  method: 'GET',
  headers: {
    Accept: 'application/vnd.vtex.ds.v10+json',
    'Content-Type': 'application/json',
    'rest-range': 'resources=0-999',
  },
}

const VerifyLogin = () => {
  const { orderForm } = useOrderForm()

  const [isLogged, setLoggedUser] = useState(false)
  const [isApproved, setIsApproved] = useState(false)
  const [isLoading, setLoading] = useState(false)

  const [nameClient, setNameClient] = useState('')
  const [emailClient, setEmailClient] = useState('')

  const verifyIsApproved = async () => {
    console.log(orderForm?.clientProfileData?.email);
    if( typeof orderForm?.clientProfileData?.email != 'undefined'){
      //setLoading(true)
      fetch(
        `/api/dataentities/CL/search?email=${orderForm?.clientProfileData?.email}&_fields=approved`,
        requestOptionsGET
      )
        .then((res) => res.json())
        .then((data) => {
          if (data.length) {
            const { approved } = data[data.length - 1]

            setIsApproved(approved)

            if (!isLogged) {
              document.body.classList.remove('approved')
              document.body.classList.remove('logged')
            } else {
              document.body.classList.add('approved')
            }
          }

          setLoading(false)
        })
    }else{
      console.log('sem email')
    }

  }

  const verifyLogin = async () => {

    fetch('/api/sessions?items=*', requestOptionsGET)
      .then((res) => res.json())
      .then((data) => {
        //setLoading(true)
        const userLogged =
          data.namespaces.profile.isAuthenticated.value === 'true'

        if (userLogged) {
          if (data.namespaces.profile.firstName) {
            const userFirstName = data.namespaces.profile.firstName.value

            setNameClient(userFirstName)
          }

          const userEmail = data.namespaces.profile.email.value
          const emailName = userEmail.split('@')

          document.body.classList.add('logged')
          document.body.classList.remove('not-login')
          setLoggedUser(true)
          setEmailClient(emailName[0])
          setLoading(false)
        } else {
          document.body.classList.remove('logged')
          document.body.classList.add('not-login')
          setLoggedUser(false)
          setLoading(false)
        }
      })
  }

  useEffect(() => {
    verifyLogin()
    verifyIsApproved()
  }, [orderForm])

  return { isLogged, isApproved, isLoading, nameClient, emailClient }
}

export default VerifyLogin
