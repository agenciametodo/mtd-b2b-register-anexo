import React, { useState } from 'react'
import { Link } from 'vtex.render-runtime'
import { Spinner } from 'vtex.styleguide'
import classNames from 'classnames'

import './styles.global.css'
import VerifyLogin from '../helper/VerifyLogin'

const LoginModule = () => {
  const dataClient = VerifyLogin()
  const { isLogged, isLoading, nameClient, emailClient } = dataClient
  const [isToggled, setToggled] = useState(false)
  //console.log(isLoading)


  return (
    <>
      {isLoading ? (
        <div className="flex items-center justify-center">
          <Spinner color="#000" />
        </div>
      ) : (
        <>
          <section
            className={classNames('profile__main', {
              '-unlogged': !isLogged,
              '-logged': isLogged,
              '-active': isToggled,
            })}
          >
            <div
              className={classNames('profile__component', {
                '-logged': isLogged,
                '-active': isToggled,
              })}
              onClick={() => setToggled(!isToggled)}
              role="presentation"
            >
              <div className="profile__icon">
                <Link to="/account#/profile">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 63.01 55.76"
                    width="40px"
                  >
                    <defs>
                      <clipPath id="clip-path" transform="translate(0 0)">
                        <rect width="63" height="56" fill="none" />
                      </clipPath>
                    </defs>
                    <title>icon-profile</title>
                    <g id="Layer_2" data-name="Layer 2">
                      <g id="Layer_1-2" data-name="Layer 1">
                        <g clipPath="url(#clip-path)">
                          <path
                            d="M32.09,0A31.64,31.64,0,0,0,11.17,55.76a17.33,17.33,0,0,1,1.55-1.07l9.39-5.14a4.07,4.07,0,0,0,2-3.34V42.39a21.7,21.7,0,0,1-3.8-7.88,3.27,3.27,0,0,1-1.43-2.63V27.7A3,3,0,0,1,20,25.31v-6.2S18.66,9.55,31.62,9.55s11.65,9.56,11.65,9.56v6.08a3.48,3.48,0,0,1,1.07,2.39v4.18a3.09,3.09,0,0,1-2.26,3A24,24,0,0,1,39,41.08c-.36.47-.71,1-1,1.19v3.94a4,4,0,0,0,2.14,3.46l10.1,5c.6.36,1.19.71,1.67,1.07A31.29,31.29,0,0,0,63,32.24,31.64,31.64,0,0,0,32.09,0Z"
                            transform="translate(0 0)"
                            fill="#fff"
                          />
                        </g>
                      </g>
                    </g>
                  </svg>
                </Link>
              </div>
              {isLogged ? (
                <div className="profile__name">
                  <Link to="/account#/profile">
                    Bem-vindo, {nameClient || emailClient}!<br />
                    <span className="colorful">Acesse sua conta</span>
                  </Link>
                </div>
              ) : (
                <>
                  <div className="profile__register">
                    Faça{' '}
                    <Link to="/login?returnUrl=%2F">
                      <span className="colorful">Login</span>{' '}
                    </Link>
                    ou{' '}
                    <Link to="/register">
                      <span className="colorful">Cadastre-se</span>
                    </Link>{' '}
                    para ver os preços
                  </div>
                </>
              )}
            </div>
          </section>
        </>
      )}
    </>
  )
}

export default LoginModule
