import React, { useState } from 'react'


import './styles.global.css'

const RegistrationComponent = () => {
  const [cpf, setCpf] = useState('')
  const [cnpj, setCnpj] = useState('')
  const [primeiroNome, setPrimeiroNome] = useState('')
  const [ultimoNome, setUltimoNome] = useState('')
  const [email, setEmail] = useState('')
  const [phone, setPhone] = useState('')
  const [fantasia, setFantasia] = useState('')
  const [estadual, setEstadual] = useState('')

  const [cep, setCep] = useState('')
  const [logradouro, setLogradouro] = useState('')
  const [numero, setNumero] = useState('')
  const [bairro, setBairro] = useState('')
  const [cidade, setCidade] = useState('')
  const [estado, setEstado] = useState('')
  //const [idUser, setIdUser] = useState('')
  const [contratosocial, setContratosocial] =  useState('')


  const [cadastroSucesso, setCadastroSucesso] = useState(false)
  const [erroCadastro, seterroCadastro] = useState(false)


  /*const requestOptionsFile = {
    method: 'POST',
    headers: {
      Accept: 'application/vnd.vtex.ds.v10+json',
      'Content-Type': 'application/json',
    },
    processData: false,
    contentType: false,
      body: contratosocial
  }*/

  const requestOptions = {
    method: 'POST',
   // method: 'PATCH',
    headers: {
      Accept: 'application/vnd.vtex.ds.v10+json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      pageCadastro: true,
      approved: false,
      firstName: primeiroNome,
      lastName: ultimoNome,
      document: cpf.replace(/[^a-zA-Z0-9]/g, ''),
      documentType: 'cpf',
      homePhone: `+55${phone.replace(/[^a-zA-Z0-9]/g, '')}`,
      email,
      corporateDocument: cnpj.replace(/[^a-zA-Z0-9]/g, ''),
      businessPhone: `+55${phone.replace(/[^a-zA-Z0-9]/g, '')}`,
      tradeName: fantasia,
      stateRegistration: estadual,
      isCorporate: true,
      isNewsletterOptIn: false,
    }),
  }

  const requestOptionsAD = {
    method: 'PATCH',
    headers: {
      Accept: 'application/vnd.vtex.ds.v10+json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      addressName: cnpj.replace(/[^a-zA-Z0-9]/g, ''),
      country: 'BRA',
      userId: email,
      postalCode: cep,
      street: logradouro,
      number: numero,
      neighborhood: bairro,
      city: cidade,
      state: estado,
      addressType: 'residential',
      receiverName: `${primeiroNome} ${ultimoNome}`,
    }),
  }

  const requestRegistrationAD = async () => {



    await fetch('/safedata/AD/documents', requestOptionsAD).then((res) => {
      if (res.ok) {
        setCadastroSucesso(true)
      } else {
        seterroCadastro(true)
      }
    })
  }

  /*const requestRegistrationFile = async (id:any) => {


    var url = 'api/dataentities/CL/documents/' + id + '/contratosocial/attachments';
    await fetch(url, requestOptionsFile).then((res) => {
      console.log(contratosocial)
      if (res.ok) {
        console.log('ok');
      } else {
        console.log('erro');
      }
    })
  }*/

  const requestRegistrationFile = async (id:any) => {
    var data = new FormData();
    data.append("contratosocial", contratosocial);
    var url = 'api/dataentities/CL/documents/' + id + '/contratosocial/attachments';

    fetch(url, {
      method: 'POST',
    headers: {
      Accept: 'application/vnd.vtex.ds.v10+json',
      contentType: 'application/json; charset=utf-8',
    },
    body:data
    }).then(function (res) {
      if (res.ok) {
        console.log("Upload feito com sucesso");
      } else if (res.status == 401) {
        console.log("Oops! ");
      }
    }, function () {
      console.log("Error submitting form!");
    });
  }
  const requestRegistrationCL = async () => {
    await fetch('/api/dataentities/CL/documents', requestOptions)
    .then(res => res.json())
    .then(data => {
      console.log(data);
      console.log(data.Id);
      if (data.Id != '') {
        //setIdUser(data.Id)
        //console.log(idUser);
        var id = data.Id.replace('CL-','')
        requestRegistrationFile(id)
        requestRegistrationAD()
      } else {
        seterroCadastro(true)
      }

    });
  }

  const handleChange = (event: any) => {
    let { value } = event.target

    value = value.replace(/\D/g, '')
    value = value.replace(/^(\d{5})(\d)/, '$1-$2')
    setCep(value)

    if (value.length >= 9) {
      fetch(`https://viacep.com.br/ws/${value}/json/`)
        .then((res) => res.json())
        .then((data) => {
          setLogradouro(data.logradouro)
          setBairro(data.bairro)
          setCidade(data.localidade)
          setEstado(data.uf)
        })
    }
  }

  const handleChangeCNPJ = (event: any) => {
    let { value } = event.target

    value = value.replace(/\D/g, '')
    value = value.replace(/(\d{2})(\d)/, '$1.$2')
    value = value.replace(/(\d{3})(\d)/, '$1.$2')
    value = value.replace(/(\d{3})(\d)/, '$1/$2')
    value = value.replace(/(\d{4})(\d)/, '$1-$2')

    setCnpj(value)
  }

  const handleChangeCPF = (event: any) => {
    let { value } = event.target

    value = value.replace(/\D/g, '')
    value = value.replace(/(\d{3})(\d)/, '$1.$2')
    value = value.replace(/(\d{3})(\d)/, '$1.$2')
    value = value.replace(/(\d{3})(\d)/, '$1-$2')

    setCpf(value)
  }

  const handleChangePhone = (event: any) => {
    let { value } = event.target

    value = value.replace(/\D/g, '')
    value = value.replace(/(\d{0})(\d)/, '$1($2')
    value = value.replace(/(\d{2})(\d)/, '$1) $2')
    value = value.replace(/(\d{5})(\d)/, '$1-$2')
    setPhone(value)
  }

  const handleChangeContrato = (event: any) => {
    if (event.target.files && event.target.files.length > 0) {
      let files = event.target.files;
      console.log(files[0]);
      console.log('file: ', contratosocial);

      setContratosocial(files[0])

 // const imagedata = event.target.files[0];
  console.log('imagedata1: ', contratosocial);

//console.log(contratosocial)
   //   console.log('imagedata: ', formData.get('inputname'));

    }else{
      console.log('nao tem')
    }

  }

  if (erroCadastro) {
    return (
      <>
        <div className="component">
          <div className="component-content--registration">
            <h2 className="component-content--registration--title">
              Ocorreu um erro
            </h2>
            <p className="component-content--registration--text">
              Parece que você ja possui cadastro ou já realizou um login antes
              de se cadastrar. Por favor entre em contato com nossa Central de
              Atendimento.
            </p>
          </div>
        </div>
      </>
    )
  }

  return (
    <>
      <div className="component">
        {!cadastroSucesso ? (
          <>
            <div className="component-content">
              <div className="component-content--top">
                <h2 className="component-content--title">Cadastro</h2>
                <p className="component-content--text">
                  Insira os dados da sua empresa para criar uma conta
                </p>

                <form encType="multipart/form-data"
                  className="component-content--form"
                  onSubmit={(event) => {
                    event.preventDefault()
                    requestRegistrationCL()
                  }}
                >
                  <div className="component-content">
                    <label
                      htmlFor="primeiroNome"
                      className="component-content-label"
                    >
                      Primeiro nome*
                    </label>
                    <input
                      id="primeiroNome"
                      name="primeiroNome"
                      className="component-content-input"
                      type="text"
                      placeholder="Digite o seu primeiro nome"
                      value={primeiroNome}
                      onChange={(event) => {
                        setPrimeiroNome(event.target.value)
                      }}
                      required
                    />
                  </div>

                  <div className="component-content">
                    <label
                      htmlFor="ultimoNome"
                      className="component-content-label"
                    >
                      Último nome*
                    </label>
                    <input
                      id="ultimoNome"
                      name="ultimoNome"
                      className="component-content-input"
                      type="text"
                      placeholder="Digite o seu primeiro nome"
                      value={ultimoNome}
                      onChange={(event) => {
                        setUltimoNome(event.target.value)
                      }}
                      required
                    />
                  </div>

                  <div className="component-content">
                    <label htmlFor="email" className="component-content-label">
                      E-mail*
                    </label>
                    <input
                      id="email"
                      name="email"
                      className="component-content-input"
                      type="email"
                      placeholder="Digite o seu e-mail"
                      value={email}
                      onChange={(event) => {
                        setEmail(event.target.value)
                      }}
                      required
                    />
                  </div>

                  <div className="component-content">
                    <label htmlFor="cnpj" className="component-content-label">
                      CNPJ*
                    </label>
                    <input
                      id="cnpj"
                      name="cnpj"
                      className="component-content-input"
                      type="cnpj"
                      maxLength={18}
                      placeholder="Digite seu CNPJ*"
                      value={cnpj}
                      required
                      onChange={(e) => handleChangeCNPJ(e)}
                    />
                  </div>

                  <div className="component-content">
                    <label
                      htmlFor="cpf"
                      className="vtex-registration-component-content-label"
                    >
                      CPF
                    </label>
                    <input
                      id="cpf"
                      name="cpf"
                      className="component-content-input"
                      type="cpf"
                      maxLength={14}
                      placeholder="Digite seu CPF*"
                      value={cpf}
                      onChange={(e) => handleChangeCPF(e)}
                    />
                  </div>

                  <div className="component-content">
                    <label htmlFor="phone" className="component-content-label">
                      Telefone*
                    </label>
                    <input
                      id="phone"
                      name="phone"
                      className="component-content-input"
                      type="phone"
                      maxLength={15}
                      placeholder="(11) 99999-9999*"
                      value={phone}
                      required
                      onChange={(e) => handleChangePhone(e)}
                    />
                  </div>

                  <div className="component-content">
                    <label
                      htmlFor="fantasia"
                      className="component-content-label"
                    >
                      Nome Fantasia*
                    </label>
                    <input
                      id="fantasia"
                      name="fantasia"
                      className="component-content-input"
                      type="text"
                      required
                      placeholder="Digite o Nome Fantasia"
                      value={fantasia}
                      onChange={(event) => {
                        setFantasia(event.target.value)
                      }}
                    />
                  </div>

                  <div className="component-content">
                    <label
                      htmlFor="estadual"
                      className="component-content-label"
                    >
                      Inscrição Estadual*
                    </label>
                    <input
                      id="estadual"
                      name="estadual"
                      className="component-content-input"
                      type="text"
                      placeholder="Digite a Inscrição Estadual"
                      value={estadual}
                      onChange={(event) => {
                        setEstadual(event.target.value)
                      }}
                    />
                  </div>

                  <h2 className="component-form-content--title">Endereço</h2>
                  <div className="component-content">
                    <label htmlFor="cep" className="component-content-label">
                      CEP*
                    </label>
                    <input
                      id="cep"
                      name="cep"
                      className="component-content-input"
                      type="cep"
                      maxLength={9}
                      required
                      placeholder="00000-000"
                      value={cep}
                      onChange={(e) => handleChange(e)}
                    />
                  </div>

                  <div className="component-content">
                    <label
                      htmlFor="logradouro"
                      className="component-content-label"
                    >
                      Logradouro*
                    </label>
                    <input
                      id="logradouro"
                      name="logradouro"
                      className="component-content-input"
                      type="text"
                      required
                      placeholder="00000-000"
                      value={logradouro}
                      onChange={(event) => {
                        setLogradouro(event.target.value)
                      }}
                    />
                  </div>

                  <div className="component-content">
                    <label htmlFor="numero" className="component-content-label">
                      Número*
                    </label>
                    <input
                      id="numero"
                      name="numero"
                      className="component-content-input"
                      type="text"
                      required
                      placeholder="000"
                      value={numero}
                      onChange={(event) => {
                        setNumero(event.target.value)
                      }}
                    />
                  </div>

                  <div className="component-content">
                    <label htmlFor="bairro" className="component-content-label">
                      Bairro*
                    </label>
                    <input
                      id="bairro"
                      name="bairro"
                      className="component-content-input"
                      type="text"
                      required
                      placeholder="Digite o bairro"
                      value={bairro}
                      onChange={(event) => {
                        setBairro(event.target.value)
                      }}
                    />
                  </div>

                  <div className="component-content">
                    <label htmlFor="cidade" className="component-content-label">
                      Cidade*
                    </label>
                    <input
                      id="cidade"
                      name="cidade"
                      className="component-content-input"
                      type="text"
                      required
                      placeholder="Digite a cidade"
                      value={cidade}
                      onChange={(event) => {
                        setCidade(event.target.value)
                      }}
                    />
                  </div>

                  <div className="component-content">
                    <label htmlFor="estado" className="component-content-label">
                      Estado*
                    </label>
                    <input
                      id="estado"
                      name="estado"
                      className="component-content-input"
                      type="text"
                      required
                      placeholder="Digite a estado"
                      value={estado}
                      onChange={(event) => {
                        setEstado(event.target.value)
                      }}
                    />
                  </div>
                  <h2 className="component-form-content--title">Contrato Social</h2>
                  <div className="component-content">
                    <label htmlFor="contrato" className="component-content-label">
                      Contrato Social
                    </label>
                    <input
                      id="contrato"
                      name="contratosocial"
                      className="component-content-input"
                      type="file"
                      onChange={(e) => handleChangeContrato(e)}
                    />

                  </div>

                  <p>
                    Ao clicar em confirmar concordo que aceito aos Termos e
                    Condições de Uso do Ecommerce; Termo Gerais da Política de
                    Privacidade
                  </p>
                  <button type="submit" className="component-content-button">
                    Confirmar
                  </button>
                </form>
              </div>
            </div>
          </>
        ) : (
          <>
            <div className="component-content--registration">
              <h2 className="component-content--registration--title">
                Cadastro realizado
              </h2>
              <p className="component-content--registration--text">
                O cadastro ainda estará sob aprovação e não será possível
                visualizar os preços no site.
              </p>
            </div>
          </>
        )}
      </div>
    </>
  )
}

export default RegistrationComponent
